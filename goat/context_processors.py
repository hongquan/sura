from urllib.parse import quote

from .models import Category

def categories(request):
    alphabet = tuple(chr(i) for i in range(ord('a'), ord('z')))
    categories = Category.objects.filter(published=True)\
                         .values_list('pk', 'name', 'slug')

    context = {
        'alphabet': (quote('#'),) + alphabet,
        'categories': categories
    }
    return context

