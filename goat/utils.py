import re

def first_words(s, num):
    matches = re.finditer(r'(\w+)\b', s, re.UNICODE)
    return ' '.join(w.group(1) for i, w in enumerate(matches) if i < num)
