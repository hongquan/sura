# Create your views here.
from django.conf.urls import patterns, url
from django.views.generic import ListView, DetailView
from .models import Word, Category


class WordListView(ListView):
    model = Word

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(published=True)



class WordDetailView(DetailView):
    model = Word



class DictView(WordListView):
    def get_queryset(self):
        queryset = super().get_queryset()
        prefix = self.kwargs['pre']
        # Prefix is 'a' to 'z'
        if prefix.isalpha():
            return queryset.filter(text__istartswith=prefix)
        # Prefix is '#'
        for i in range(ord('a'), ord('z')):
            queryset = queryset.exclude(text__istartswith=chr(i))
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dict'] = self.kwargs['pre']
        return context



class CategoryView(WordListView):
    def get_queryset(self):
        queryset = super().get_queryset()
        cat = Category.objects.get(pk=self.kwargs['pk'])
        return queryset.filter(category=cat)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cat'] = self.kwargs['pk']
        return context
