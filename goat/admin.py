from django.contrib import admin

from .models import Word, Example, Category
from .forms import WordForm, CategoryForm


class ExampleInline(admin.TabularInline):
    model = Example

class WordAdmin(admin.ModelAdmin):
    form = WordForm
    inlines = (ExampleInline,)


class CategoryAdmin(admin.ModelAdmin):
    form = CategoryForm


admin.site.register(Word, WordAdmin)
admin.site.register(Category, CategoryAdmin)
