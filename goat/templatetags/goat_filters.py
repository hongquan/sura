from urllib import parse

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def unquote(value):
    return parse.unquote(value)


@register.filter
def active_if_equal(text, text2):
    return 'active' if str(text) == str(text2) else ''
