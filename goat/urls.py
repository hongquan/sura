
from django.conf.urls import patterns, url

from .views import WordListView, WordDetailView,\
                   DictView, CategoryView

urlpatterns = patterns('',
    url(r'^$', WordListView.as_view(), name='index'),
    url(r'^word/(?P<pk>\d+)/(?P<slug>[-\w]+)/$', WordDetailView.as_view(), name='word-detail'),
    url(r'^dict/(?P<pre>[#%0-9a-z]+)/$', DictView.as_view(), name='dict'),
    url(r'^cat/(?P<pk>\d+)/(?P<slug>[-\w]+)/$', CategoryView.as_view(), name='cat'),
)
