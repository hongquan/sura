from django.db import models
from djorm_pgarray.fields import ArrayField
from djorm_expressions.models import ExpressionManager
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from .utils import first_words


# Ref: http://www.craigkerstiens.com/2012/11/06/django-and-arrays/

class SlugMixIn:
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super().save(*args, **kwargs)


class Word(models.Model):
    text = models.CharField(max_length=160)
    slug = models.CharField(max_length=160)
    definition = models.TextField()
    pub_date = models.DateField('Published on', null=True, blank=True)
    tags = ArrayField(dbtype='varchar(160)', null=True, blank=True)
    author = models.ForeignKey(User, null=True, blank=True)
    category = models.ForeignKey('Category', null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    published = models.BooleanField(default=False)
    objects = ExpressionManager()


    def __str__(self):
        return self.text


    def save(self, *args, **kwargs):
        self.slug = slugify(self.text)
        return super().save(*args, **kwargs)


class Example(models.Model):
    text = models.TextField()
    word = models.ForeignKey(Word)

    def __str__(self):
        return '{}:{}'.format(self.word.text, first_words(self.text, 3))


class Category(SlugMixIn, models.Model):
    name = models.CharField(max_length=160)
    slug = models.CharField(max_length=160)
    created_date = models.DateTimeField(auto_now_add=True)
    published = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


