from django import forms
from .models import Word, Category

# This field is to used with djorm_pgarray's ArrayField
class TagsArrayField(forms.CharField):
    def to_python(self, value):
        text = super(TagsArrayField, self).to_python(value)
        # text is the string we get from TextInput widget.
        # Wrap with {} to comply with PostgreSQL syntax to
        # store to database
        return '{{{0}}}'.format(text)

    def prepare_value(self, value):
        # value is list of string,
        # we join them with commas
        # to show on TextInput widget
        commajoined = ', '.join(value) if value else value
        return super(TagsArrayField, self).prepare_value(commajoined)



class WordForm(forms.ModelForm):
    tags = TagsArrayField()

    class Meta:
        model = Word
        exclude = ('pub_date', 'slug')


class CategoryForm(forms.ModelForm):
    class Meta():
        model = Category
        exclude = ('pub_date', 'slug')
